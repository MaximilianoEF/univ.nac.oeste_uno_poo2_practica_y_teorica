package Figuras;

public interface Desplazable {
	
	public void desplazar(double deltaX, double detaY);

}
