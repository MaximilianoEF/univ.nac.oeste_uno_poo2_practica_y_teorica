package Ejercicios.Semana3.pedregal;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Pedregal {

	
	public void sePuede(ArrayList<Cuadricula> terreno, int lado, int frente) throws IOException {
		ArrayList<Cuadricula> arrayConsecutivos=new ArrayList<Cuadricula>();
		
		double dimensionCasa=lado*frente;
		
		for(Cuadricula tmp: terreno) {
			
			if(tmp.getEstadoCuadricula().equals("DISPONIBLE")) {
				
				arrayConsecutivos.add(tmp);
			}else {
				
				arrayConsecutivos.clear();
			}
			
			if(arrayConsecutivos.size()==dimensionCasa) {
				
				crearArchivos(arrayConsecutivos);
			}
           if(arrayConsecutivos.size()<dimensionCasa) {
				
				crearArchivos2();
			}
			
		}
	}
	
public void crearArchivos(ArrayList<Cuadricula> posicionesOcupadas) throws IOException {
		int dimension=posicionesOcupadas.size();
		
		//CREAR ARCHIVO ENTRADA
		FileWriter flwriter = null;
		try {
			//crea el flujo para escribir en el archivo
			
			flwriter = new FileWriter("Archivos/pedregal.out");
			
			//crea un buffer o flujo intermedio 
			
			BufferedWriter bfwriter = new BufferedWriter(flwriter);
			
			bfwriter.write(posicionesOcupadas.get(0).getPosicion().getX()+ " "+posicionesOcupadas.get(dimension).getPosicion().getY()+" ");
			
			
			//cierra el buffer intermedio
			bfwriter.close();
 
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {//cierra el flujo principal
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}}
	
		public void crearArchivos2() throws IOException {
			
			
			//CREAR ARCHIVO ENTRADA
			FileWriter flwriter = null;
			try {
				//crea el flujo para escribir en el archivo
				
				flwriter = new FileWriter("Archivos/pedregal.out");
				
				//crea un buffer o flujo intermedio 
				
				BufferedWriter bfwriter = new BufferedWriter(flwriter);
				
				bfwriter.write("NO");
				
				
				//cierra el buffer intermedio
				bfwriter.close();
	 
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (flwriter != null) {
					try {//cierra el flujo principal
						flwriter.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
	
	
	
}}


