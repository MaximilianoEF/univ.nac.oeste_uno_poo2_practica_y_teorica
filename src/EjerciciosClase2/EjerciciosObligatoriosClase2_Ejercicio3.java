package EjerciciosClase2;

import java.util.Scanner;

public class EjerciciosObligatoriosClase2_Ejercicio3 {

	/*
	 * Ejercicio 3:

Escribir un m�todo que reciba como par�metro una matriz cuadrada de n�meros reales y devuelva true (o false), si todos los elementos
 (fuera de la diagonal principal) est�n por encima de la media de los elementos de la diagonal principal.
	 * 
	 * */
	
	public int obtenerOrdenMatriz() {
		
		
		System.out.println("Ingrese el orden de la Matriz Cuadrada");
		Scanner sc= new Scanner(System.in);
		
		int orden=sc.nextInt();
		
		return orden;
		
		
	}
	public  int [][] crearMatriz(int orden) {
		int i;
		int j;
		
		
		int matriz[][] = new int[orden][orden];
		
		for(i=0;i<matriz.length;i++) {
			for(j=0;j<matriz.length;j++) {
				
				int valor = (int) Math.floor(Math.random()*6+1);
				
				matriz[i][j]=valor;
			}
			
		}
		return matriz;
		
	
		
	}
	
	public int calcularMediaDiagonalPrincipal(int matriz[][], int orden) {
		
		int mediaDiagonal;
		int sumaDiagonalPrincipal=0;
		int i;
		
		for(i=0;i<matriz.length;i++) {
			
			sumaDiagonalPrincipal+=matriz[i][i];
			
		}
		
		mediaDiagonal=sumaDiagonalPrincipal/orden;
		
		return mediaDiagonal;
		
		
	}
	
	public boolean mayorOmenorAmediaDiagonal(int matriz[][], int mediaDiagonal) {
		
		int i,j;
		boolean valor=true;
		int menor;
		int valortmp=0;
		 
		for(i=0;i<matriz.length;i++) {
			for(j=0;j<matriz.length;j++) {
				
				if(i!=j) {
					 valortmp=matriz[i][j];
				}
				
				
				if(valortmp<mediaDiagonal) {
					valor =false;
					break;}
	
		}}
		
		return valor;
	
	}
	
	public void mostrarMatriz(int matriz[][]) {
		
		int i,j;
		for(i=0;i<matriz.length;i++) {
			System.out.println();
			for(j=0;j<matriz.length;j++) {
				System.out.print(matriz[i][j]+ "  ");
			}
		}
		
	}
	
	
	public static void main(String[] args) {
		
		EjerciciosObligatoriosClase2_Ejercicio3 E3 = new EjerciciosObligatoriosClase2_Ejercicio3();
		
		int orden=E3.obtenerOrdenMatriz();
		
		int matriz2[][] = new int[orden][orden];
		
		matriz2=E3.crearMatriz(orden);
		
		E3.mostrarMatriz(matriz2);
		
		System.out.println();
		
		int media=E3.calcularMediaDiagonalPrincipal(matriz2, orden);
		
		System.out.println("La media es: "+ media);
		
		boolean valor=E3.mayorOmenorAmediaDiagonal(matriz2, media);
		
		if(valor==true) {
			System.out.println("todos los elementos fuera de la diagonal principal est�n por encima de la media  de la diagonal principal ");}
		
		else {
			System.out.println("no todos los elementos fuera de la diagonal principal estan por encima de la media de la diagonal principal");
		}
		
	
		
	}
	 
}

