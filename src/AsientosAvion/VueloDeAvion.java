package AsientosAvion;

public class VueloDeAvion {
	
	/*
	 * Implementar la clase VueloDeAvion con las operaciones. Indique pre y post condiciones de las operaciones.
 public class VueloDeAvion {
 public VueloDeAvion(int cantidadDeAsientos)
 public void vender(int numeroDeAsiento)
 public void reservar(int[] numerosDeAsientosReservados)
 public EstadoAsiento consultarEstado(int numeroDeAsiento)
 public int contarAsientosDisponibles()
 public int contarAsientosVendidosEntre(int desdeNumeroAsiento, int hastaNumeroAsiento)
 public int[] obtenerNumerosDeAsientosDisponibles()
}

public enum EstadoAsiento { 
 DISPONIBLE, RESERVADO, VENDIDO 
};*/
	
	
	private int cantidadDeAsientos;
	private Asiento [] asientosAvion;
	

	public VueloDeAvion(int cantidadDeAsientos) {
		super();
		this.setCantidadDeAsientos(cantidadDeAsientos);
		Asiento [] asientosAvion=new Asiento [cantidadDeAsientos];
		
	}

	public int getCantidadDeAsientos() {
		return cantidadDeAsientos;
	}

	public void setCantidadDeAsientos(int cantidadDeAsientos) {
		this.cantidadDeAsientos = cantidadDeAsientos;
	}
	
	public void vender(int numeroDeAsiento, int dniPasajero,String nombrePasajero) {
		
		Asiento aTmp=new Asiento(numeroDeAsiento, EstadoAsiento.VENDIDO );
		asientosAvion[numeroDeAsiento]=aTmp;
		Pasajero pasajero=new Pasajero(dniPasajero, nombrePasajero);
		aTmp.setPasajero(pasajero);
		
		
	}
	public void reservar(int[] numerosDeAsientosReservados) {
		
		for(Asiento tmp: asientosAvion) {
			if(tmp.geteAsiento().equals(EstadoAsiento.DISPONIBLE)) {
				tmp.seteAsiento(EstadoAsiento.RESERVADO);
			numerosDeAsientosReservados[numerosDeAsientosReservados.length+1 ]=tmp.getaNumero();
			
			}
			break;
		}
	}
	
	public EstadoAsiento consultarEstado(int numeroDeAsiento) {
		return this.asientosAvion[numeroDeAsiento].geteAsiento();}
	
	public int contarAsientosDisponibles() {
		int asientosDisponibles=0;
		
		for(Asiento tmp: asientosAvion) {
			if(tmp.geteAsiento().equals(EstadoAsiento.DISPONIBLE)  ) {
				asientosDisponibles++;
				
			}
		}
		
		return asientosDisponibles;}
	
	public int contarAsientosVendidosEntre(int desdeNumeroAsiento, int hastaNumeroAsiento) {
int asientosVendidos=0;
int i;
		
		for(i=desdeNumeroAsiento;i<=hastaNumeroAsiento;i++) {
			if(this.asientosAvion[i].geteAsiento().equals(EstadoAsiento.DISPONIBLE)  ) {
				asientosVendidos++;
				
			}
		}
		
		return asientosVendidos;}
	
	 public int[] obtenerNumerosDeAsientosDisponibles() {
		 int cont=0;
		 int[] numerosDisponibles= new int [contarAsientosDisponibles()];
		 
		 for (Asiento tmp: asientosAvion) {
			 if(tmp.geteAsiento().equals(EstadoAsiento.DISPONIBLE)) {
				 numerosDisponibles[cont]=tmp.getaNumero();
				 cont++;
				 
			 }
		 }
		 
		 
		return numerosDisponibles;}
	
	
	
	
	

}
