package ProgramaProbador;

public class Paciente {
	
	private int numHistoria;
	private String nombre;
	
	public Paciente(int numHistoria, String nombre) {
		this.numHistoria = numHistoria;
		this.nombre = nombre;
	}
	
	public int getNumHistoria() {
		return numHistoria;
	}
	
	public void setNumHistoria(int numHistoria) {
		this.numHistoria = numHistoria;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
}