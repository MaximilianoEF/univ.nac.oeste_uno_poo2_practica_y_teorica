package InstalandoAplicaciones;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class PruebaStress {
	

		public static void crearArchivosDeEntrada() throws IOException {
			PrintWriter salida = new PrintWriter(new FileWriter("Archivo/almacenamiento.in")); 
		    int numero;
			salida.print(5000);
			salida.print(" ");
			salida.println(1000);
			
			for(int i=0; i<5000; i++) {
				numero = (int) (Math.random() * 100) + 1;
				salida.print(numero);
			    salida.print(" ");
			}
			
			salida.close(); 
			}
		
		
		public static void main(String[] args) throws IOException, NumberFormatException, RestriccionVioladaException {
			
			long TInicio, TFin, tiempo; //Variables para determinar el tiempo de ejecuci�n
			
			crearArchivosDeEntrada();
			
			TInicio = System.currentTimeMillis(); //Tomamos la hora en que inicio el algoritmo y la almacenamos en la variable inicio
			
			UninstallApp ua=new UninstallApp("Archivo/almacenamiento.in");
			
			TFin = System.currentTimeMillis(); //Tomamos la hora en que finaliz� el algoritmo y la almacenamos en la variable T
			tiempo = TFin - TInicio; //Calculamos los milisegundos de diferencia
			System.out.println("Tiempo de ejecuci�n en milisegundos: " + tiempo); //Mostramos en pantalla el tiempo de ejecuci�n en milisegundos
			
		}

	}
	
