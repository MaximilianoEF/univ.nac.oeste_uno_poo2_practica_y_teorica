package Vehiculo;

public class Motocicleta extends Vehiculo {
private Persona acompaņante;
	
	public Motocicleta(double km, Persona chofer) {
		super(km, chofer);
	}
	
	public void subirAcompaņante(Persona acompaņante) {
		this.acompaņante = acompaņante;
	}
	
	public Persona bajarAcompaņante() {
		return acompaņante = null;
	}
	 
	public void cambiarChofer(Persona chofer) {
		
		if(acompaņante == null) {
			this.asignarChofer(chofer);
		}
		else {
			System.out.println("Hay un acompaņante.");
			System.out.println("No se puede cambiar el chofer.");
		}
		
		
	}

	
}
